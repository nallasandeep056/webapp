package org.practice.webClasses;

import java.util.HashMap;

import org.practice.webApp.dto.User;

public class LoginClass {
	
	HashMap<String,String> user = new HashMap<String,String>();
	
	public LoginClass() {
		user.put("sandeep", "Sandeep");
		user.put("vidya", "Vidyanath");
		user.put("raj", "Raj kumar");
	}
	
	public User getUserDetails(String userId) {
		User user1 = new User();
		user1.setUserId(userId);
		user1.setUserName(user.get(userId));
		return user1;
	}
	
	
	public boolean authenticate(String userId, String password) {
		if(password == null || password.trim() == "") {
			return false;
		}
		return true;
	}
}
