<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Process page</title>
</head>
<body>
<jsp:useBean id="user" class="org.practice.webApp.dto.User" scope="request">
	<jsp:setProperty property="*" name="user"/>		
</jsp:useBean>
<h3>Hi
<jsp:getProperty property="userName" name="user"/>!
</h3><br>
Address line 1: <jsp:getProperty property="address1" name="user"/><br>
Address line 2: <jsp:getProperty property="address2" name="user"/><br>
City: <jsp:getProperty property="city" name="user"/><br>
Pin code: <jsp:getProperty property="pin" name="user"/><br>
State: <jsp:getProperty property="state" name="user"/><br>
</body>
</html>