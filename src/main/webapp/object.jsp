<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Page context Object</title>
</head>
<body>

<%
String userName = request.getParameter("name");
if (userName != null){
	session.setAttribute("sessionUserName", userName);
	application.setAttribute("appUserName", userName);
	pageContext.setAttribute("pageContextUserName", userName, PageContext.APPLICATION_SCOPE);
}
%>

User name is: <%=userName %><br>
Session user name is <%=session.getAttribute("sessionUserName") %><br>
Application user name is <%=application.getAttribute("appUserName") %><br>
Page context user name is <%=pageContext.getAttribute("pageContextUserName", PageContext.APPLICATION_SCOPE) %>
</body>
</html>