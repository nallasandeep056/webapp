<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="org.practice.webApp.dto.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success</title>
</head>
<body>
<%--  <% User user = (User) request.getAttribute("user");%> --%>
 <jsp:useBean id="user" class="org.practice.webApp.dto.User" scope="request">
 	<%-- <jsp:setProperty property="user" name="userName" value="New user"/> --%>
 </jsp:useBean>
<h3>Authentication is successful!!
<br>Hi <jsp:getProperty name="user" property="userName" />!

</h3>
</body>
</html>